S3I Package
===========
.. _BlockingConnection: https://pika.readthedocs.io/en/stable/modules/adapters/blocking.html
.. _PlainCredentials: https://pika.readthedocs.io/en/stable/modules/credentials.html#plaincredentials
.. _ConnectionParameters: https://pika.readthedocs.io/en/stable/modules/parameters.html
.. _BlockingChannel: https://pika.readthedocs.io/en/stable/modules/adapters/blocking.html#pika.adapters.blocking_connection.BlockingChannel
.. _Channel: https://pika.readthedocs.io/en/stable/modules/channel.html
.. _ResponseObject: https://www.w3schools.com/python/ref_requests_response.asp
.. _docs: https://pika.readthedocs.io/en/stable/ 
.. _RFC4880: https://tools.ietf.org/html/rfc4880

The S3I package uses the Pika package, which enables access to the Broker. More about Pika is provided in its docs_.

.. toctree::
   :maxdepth: 2
   :titlesonly:
   :caption: Contents:
   
.. automodule:: s3i.identity_provider                                           
    :members:
    :special-members: __init__

.. automodule:: s3i.directory                                           
    :members:
    :special-members: __init__   

.. automodule:: s3i.broker                                           
    :members:
    :special-members: __init__

.. automodule:: s3i.config                                           
    :members:
    :special-members: __init__
   
.. automodule:: s3i.messages                                           
    :members:
    :special-members: __init__

.. automodule:: s3i.key_pgp                                           
    :members:
    :special-members: __init__

.. automodule:: s3i.event_system
	:members: 
	:special-members: __init__