# Preliminaries

## Requirements
* Python 3+
* recommended: virtualenv and optionally, but also recommended virtualenvwrapper
To install virtualenv and virtualenvwrapper, just execute (after installing Python):
```
pip install virtualenv
pip install virtualenvwrapper-win
```

More Information on how to use virtualenv can be found here: [The Hitchhiker's Guide to Python](http://docs.python-guide.org/en/latest/dev/virtualenvs/)

## Preliminaries
We recommend creating a virtual environment for the project (`mkvirtualenv projectname`). After creating the environment, activate it (`workon projectname`). Navigate to the root folder containing the `requirements.txt` and install the required modules for the demo with (`pip install -r requirements.txt`).


## Folders
Three folders located in directory ../demo/ are neccessary to replay the demos: 

```
demo_send_data
demo_received_data
key
```
The *demo_send_data* and *demo_received_data* folders contain the data files which can be send or are received during the demos, respectively. The *key* folder contains the private keys to encrypt the PGP messages in demo 4. More information on sent and received example files can be found in the position paper available at [https://www.kwh40.de/veroffentlichungen/](https://www.kwh40.de/veroffentlichungen/).
