******************
Demo 7 - Get Token
******************

Get a token from S3I IdentityProvider
=====================================

This short Python code (*demo_get_token.py*) lets you easily receive a JWT token from S3I IdentityProvider. Just follow the script as it asks for your credentials (person identity). If you want, you can also specify a thing identity by providing a client and its secret. If not, the *admin-cli* client is used that can be used throughout S3I, as well. Optionally, you can request more or fewer scope claims to appear in access token by providing client scope(s). The client scopes are referenced by their name and must be linked to above provided client in S3I IdentityProvider.    


.. toctree::
   :maxdepth: 2
   :titlesonly:
   :caption: Contents:

.. automodule:: demo.demo_get_token
    :members:
