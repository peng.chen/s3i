*********************
Demo 8 - Event System
*********************

Activate an event system at python-based gSFL things
====================================================

In demo 8, an event system is implemented at a python-based gSFL Harvester. a python-based sawmill sends a S³I eventSubscriptionRequest included a RQL-based filter expression in order to subscribe to an event. To acknowledge the subscription, the sawmill will obtain a S³I eventSubscriptionReply with a subscription id which specifies the subscription. The sawmill will then receive a S³I EventMessage, when the filter is set to True. To unsubscribe an event, the sawmill sends a S³I EventUnsubscriptionRequest containing a subscription id. Run the following Python Scripts to replay this demo:

1. demo8_event_server.py simulates a python based gSFL Harvester with event system implemented. It starts a dummy service function which decrease the rpm of harvester. A listener is also started to wait for a S³I EventSubscriptionRequest or EventUnsubscriptionRequest. Events are handled in the Event Manager.
    •   Press the right alt on the keyboard to exit the program.
2. demo8_event_client.py simulates a python based sawmill which subscribes an event by Harvester.
    •   Enter an RQL filter expression, e.g. lt(features/id1/properties/rpm, 2800) which means, an event is triggerd when the rpm is less than 2800.
    •   Press the left alt on the keyboard to send a S³I EventUnsubscriptionRequest to Harvester

.. toctree::
   :maxdepth: 2
   :titlesonly:
   :caption: Contents:

.. automodule:: demo.demo8_event_server
    :members:

.. automodule:: demo.demo8_event_client
    :members:

