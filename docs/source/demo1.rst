*********************
Demo 1 - User to user
*********************

Message exchange between persons
================================

In demo 1, a forest owner (sender) sends an S3I-B user message to a forest expert (receiver). Run the following Python scripts to replay this demo:

1.	demo1_receiver_hmi.py simulates the receiver’s human machine interface (HMI), e.g., an app. It starts a listener waiting for messages from the sender until it is stopped manually. 
   •	Enter username/password for forest expert
2.	demo1_sender_hmi.py simulates the sender’s HMI and sends messages to the receiver 
   •	Enter username/password for forest owner
   •	Enter subject and text 
   •  Optional: Enter the file names of attachments which are located in the folder demo_send_data (space separated list)

.. toctree::
   :maxdepth: 2
   :titlesonly:
   :caption: Contents:
   
.. automodule:: demo.demo1_receiver_hmi
    :members:
   
.. automodule:: demo.demo1_sender_hmi
    :members:
   
