from s3i import IdentityProvider, Directory, Broker, UserMessage, TokenType
import uuid
import json
import jwt
import base64
import time
import os


if __name__ == "__main__":
    """
    Main function for demo 1: prepare and send a message to receiver.

    """

    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()
                                   ) + "]: Demo 1, please log in as forest owner (franz_maier)!")
    username = input('[S3I]: Please enter the username: \n')
    password = input('[S3I]: Please enter the password: \n')

    '''
    grant type as password is a simplified way to get a JWT. In a subsequent version, grant type as authorization code flow will be implemented.
    '''
    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()
                                   ) + "]: Username and password are sent to S3I IdentityProvider.")
    s3i_identity_provider = IdentityProvider(grant_type='password', identity_provider_url="https://idp.s3i.vswf.dev/", realm='KWH',
                                             client_id="s3i:2aafd97c-ff05-42b6-8e4d-e492330ec959", client_secret="f584c77e-e0b6-4736-831b-ccf47ab23a65", username=username, password=password)
    access_token = s3i_identity_provider.get_token(TokenType.ACCESS_TOKEN)

    '''
    decode the access token
    verify = False means read the claimset of a JWT without performing validation of the signature or any of the registered claim names
    '''

    parsed_username = jwt.decode(access_token, verify=False)[
        "preferred_username"]
    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S",
                                   time.localtime()) + "]: Token received, " + parsed_username + " logged in.")

    '''
    authentication with JWT in S3I Directory ...
    '''
    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()
                                   ) + "]: Authentication with token in S3I Directory")
    s3i_directory = Directory(
        s3i_dir_url="https://dir.s3i.vswf.dev/api/2/", token=access_token)

    '''
    search the default HMI of the receiver by first searching his DT based based on his name in S3I Directory
    '''
    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S",
                                   time.localtime()) + "]: Search the endpoint of receiver's HMI")

    dt_schmitz = s3i_directory.queryAttributeBased(
        key='thingStructure/links/target/values/value', value="Schmitz")  # data model of person's dt is knwon

    hmi_schmitz_id = dt_schmitz[0]["attributes"]["defaultHMI"]

    '''
    the endpoint and thing id of sender's HMI are known
    '''

    sender_endpoint = "s3ib://s3i:2aafd97c-ff05-42b6-8e4d-e492330ec959"
    sender = "s3i:2aafd97c-ff05-42b6-8e4d-e492330ec959"

    '''
    query the endpoint and id of receiver HMI
    '''

    hmi_schmitz = s3i_directory.queryThingIDBased(thingID=hmi_schmitz_id)
    hmi_schmitz_endpoint_list = hmi_schmitz["attributes"]["defaultEndpoints"]

    '''
    search endpoint with "s3ibs://..."
    '''
    receiver_endpoints = list()

    for i in range(len(hmi_schmitz_endpoint_list)):
        if "s3ibs://" not in hmi_schmitz_endpoint_list[i]:
            continue
        else:
            receiver_endpoints.append(hmi_schmitz_endpoint_list[i])

    receivers = list()
    receivers.append(hmi_schmitz_id)

    '''
    authentication with access token (JWT) at S3I Broker
    In a subsequent version, authentication with JWT from S3I IdP will be implemented.
    '''

    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()
                                   ) + "]: Authentication with access token (JWT) in S3I Broker")
    demo_waldarbeiter = Broker(auth_form="Username/Password", username=" ", password=access_token,
                               host="rabbitmq.s3i.vswf.dev"
                               )

    '''
    prepare the message
    '''

    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()
                                   ) + "]: Please write your message")

    subject = input('[S3I]: please enter the subject: \n')
    text = input('[S3I]: please enter the text: \n')
    msg_uuid = "s3i:" + str(uuid.uuid4())

    filename_list = list(map(str, input(
        "[S3I]: Please enter the name of file that you want to send: ").split()))
    attachments = list()

    for i in range(len(filename_list)):
        attachment_dict = dict()
        attachment_dict["filename"] = filename_list[i]
        path_attachments = os.path.join(
            os.path.dirname(__file__), "demo_send_data", filename_list[i])
        with open(path_attachments, "rb") as f:
            # encode an attachment file to BASE64 bytes
            base64_data = base64.b64encode(f.read())
            data = base64_data.decode()  # convert byte to str
            attachment_dict["data"] = data
            attachments.append(attachment_dict)
            f.close()

    uMsg = UserMessage()
    uMsg.fillUserMessage(sender, receivers, sender_endpoint,
                         subject, text, msg_uuid, attachments=attachments)

    '''
    send the message
    '''
    demo_waldarbeiter.send(receiver_endpoints, uMsg.msg.__str__())
    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S",
                                   time.localtime()) + "]: Message has been sent")
