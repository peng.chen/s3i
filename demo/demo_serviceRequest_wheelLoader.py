from s3i import IdentityProvider, Directory, Broker, ServiceRequest, TokenType
import json
import uuid
import jwt
import time
import os
import base64


def callback(ch, method, properties, body):
    """
    This function will be called and executed every time that S3I Broker delivers messages to the consuming application (HMI). \n
    Result contained in received service reply will be parsed and printed.

    Args:
        ch: channel used during consuming, which binds productor and consumer \n
        method: „method“ contains meta information regarding message delivery: It provides delivery flags, redelivery flags, and a routing key that can be used to acknowledge the message. \n
        properties: properties of queue and exchange which are used during consuming \n
        body: content of message
    """

    body_str = body.decode('utf8').replace("'", '"')
    body_json = json.loads(body_str)
    ch.basic_ack(method.delivery_tag)
    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) +
          "]: Service reply is received, result: " + str(body_json["results"]))
    ch.stop_consuming()


if __name__ == "__main__":

    """
    Main function for demo: send a service request to the DT of the wheel loader and wait for the result.

    """

    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) +
          "]: DEMO Wheel loader (service call), please log in as forest expert! (Sachverstaendiger)")

    '''
    username and password input
    '''

    username = input('[S3I]: Please enter the username: \n')
    password = input('[S3I]: Please enter the password: \n')
    
    '''
    query for access token (JWT) from S3I IdentityProvider 
    '''

    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()
                                   ) + "]: Username and password are sent to S3I IdentityProvider.")
    s3i_identity_provider = IdentityProvider(grant_type='password', identity_provider_url="https://idp.s3i.vswf.dev/", realm='KWH',
                                             client_id="s3i:6f58e045-fd30-496d-b519-a0b966f1ab01", client_secret="475431fd-2c6d-4cae-bdfa-87226fff0cef", username=username, password=password)
    access_token = s3i_identity_provider.get_token(TokenType.ACCESS_TOKEN)

    '''
    decode the access token
    '''

    parsed_username = jwt.decode(access_token, verify=False)[
        "preferred_username"]

    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S",
                                   time.localtime()) + "]: Token received " + parsed_username + " logged in.")

    '''
    authentication with JWT in S3I Directory 
    '''

    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()
                                   ) + "]: Authentication with Token in S3I Directory")

    s3i_directory = Directory(
        s3i_dir_url="https://dir.s3i.vswf.dev/api/2/", token=access_token)

    '''
    the ids and endpoints of sender's (forest expert) HMI and the receiver (wheel loader) are known  
    '''

    sender = "s3i:6f58e045-fd30-496d-b519-a0b966f1ab01"
    sender_endpoint = "s3ibs://s3i:6f58e045-fd30-496d-b519-a0b966f1ab01"
    sender_endpoint = s3i_directory.queryThingIDBased(
        sender+"/attributes/defaultEndpoints")[0]
    wheelloaderID = s3i_directory.queryAttributeBased(
        "name", "wheelloader")[0]["thingId"]

    serviceType = "currentJobsDescription"
    wheelloaderServiceEndpoint = s3i_directory.queryEndpointService(
        wheelloaderID, serviceType)

    receivers = [wheelloaderID]
    receiver_endpoints = wheelloaderServiceEndpoint
    # receiver_endpoints.append(
    #    "s3ibs://s3i:7f654f13-11ac-413d-841c-10a8e431fc35")

    '''
    authentication and authorization with access token (JWT) at S3I Broker 
    '''

    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()
                                   ) + "]: Authentication with access token at S3I Broker")

    demo_sachverstaendiger_app = Broker(
        auth_form='Username/Password', username=" ", password=access_token, host="rabbitmq.s3i.vswf.dev")
    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S",
                                   time.localtime()) + "]: Prepare the service request.")

    parameters = dict()

    msg_uuid = "s3i:" + str(uuid.uuid4())

    '''
    create service request
    '''
    servReq = ServiceRequest()
    servReq.fillServiceRequest(senderUUID=sender, receiverUUIDs=receivers, sender_endpoint=sender_endpoint,
                               serviceType="ml40::CurrentTask", parameters=parameters, msgUUID=msg_uuid)
    '''
    send the service request message
    '''

    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S",
                                   time.localtime()) + "]: Sending the service request ")
    demo_sachverstaendiger_app.send(
        receiver_endpoints=receiver_endpoints, msg=servReq.msg.__str__())

    '''
    wait for the reply of service
    '''
    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S",
                                   time.localtime()) + "]: Wait for the service reply...")
    demo_sachverstaendiger_app.receive(
        queue=sender_endpoint, callback=callback)
