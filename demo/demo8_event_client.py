from s3i import IdentityProvider, TokenType, Broker, EventSubscriptionRequest, EventUnsubscriptionRequest
import uuid
import ast
import json
from sys import stdout
from pynput import keyboard


SAEGEWERK_ENDPOINT = "s3ib://s3i:2073c475-fee5-463d-bce1-f702bb06f899"
SAEGEWERK_CLIENT_ID = "s3i:2073c475-fee5-463d-bce1-f702bb06f899"
SAEGEWERK_CLIENT_SECRET = "2a5bd2a0-0aa0-4276-92ce-ebd3dc9813a0"

HARVESTER_ID = "s3i:7f08eb3a-0b01-4466-bd0b-438c3a1e47e5"
HARVESTER_ENDPOINT = "s3ib://s3i:7f08eb3a-0b01-4466-bd0b-438c3a1e47e5"

alt_pressed = False
subscription_id = ""


class bcolors:
    """colors for the console log"""
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def subscribe_event(filter_expression: str) -> str:
    """
    Function to prepare an eventSubscriptionRequest

    :param filter_expression: RQL filter expression
    :type filter_expression: str
    :return: eventSubscriptionRequest
    :rtype: str

    """
    event_sub_req = EventSubscriptionRequest()

    event_sub_req.fillEventSubscriptionRequest(sender=SAEGEWERK_CLIENT_ID,
                                               receivers=[HARVESTER_ID],
                                               msg_id=str(uuid.uuid4()),
                                               sender_endpoint=SAEGEWERK_ENDPOINT,
                                               rql=filter_expression)
    return event_sub_req.msg.__str__()


def unsubscribe_event(subscription_id: str) -> str:
    """
    Function to prepare an eventUnsubscriptionRequest

    :param subscription_id: subscription id
    :type subscription_id: str

    :return: eventUnsubscriptionRequest
    :rtype: str

    """
    event_unsub_req = EventUnsubscriptionRequest()
    event_unsub_req.fillEventUnsubscriptionRequest(sender=SAEGEWERK_CLIENT_ID,
                                                   receivers=[HARVESTER_ID],
                                                   msg_id=str(uuid.uuid4()),
                                                   sub_id=subscription_id,
                                                   sender_endpoint=SAEGEWERK_ENDPOINT,
                                                   )
    return event_unsub_req.msg.__str__()


def receive_sub_reply():
    """
    Function to receive eventSubscriptionReply

    :return: status of request
    :rtype: bool

    """
    global subscription_id
    while True:
        body = saegewerk_broker.receive_once(queue=SAEGEWERK_ENDPOINT)
        if body is None:
            continue
        try:
            body_json = ast.literal_eval(body.decode('utf-8'))
        except ValueError as e:
            body_json = json.loads(body)
        if body_json.get("messageType") == "eventSubscriptionReply":
            if body_json.get("ok"):
                subscription_id = body_json.get("subscription-id")
                print(
                    "[S³I] Get a event subscription reply. You have subscribed a event with sub id:{0}{1}{2}".format(
                        bcolors.UNDERLINE, subscription_id, bcolors.ENDC))
                return True
            else:
                print("[S³I] Get a event subscription reply. Your rql specification is invalid")
                return False

        return False


def receive_event_msg():
    """
    Function to receive event message

    """
    body = saegewerk_broker.receive_once(queue=SAEGEWERK_ENDPOINT)
    if body is None:
        return None
    try:
        body_json = ast.literal_eval(body.decode('utf-8'))
    except ValueError as e:
        body_json = json.loads(body)

    if body_json.get("messageType") == "eventMessage":
        stdout.write("\r[S³I] Get a event message:" + bcolors.OKBLUE +" %s" % body_json.get("content") + bcolors.ENDC)


def receive_unsub_reply():
    """
    Function to receive eventUnsubscriptionReply

    """
    while True:
        body = saegewerk_broker.receive_once(queue=SAEGEWERK_ENDPOINT)
        if body is None:
            continue
        try:
            body_json = ast.literal_eval(body.decode('utf-8'))
        except ValueError:
            body_json = json.loads(body)
        if body_json.get("messageType") == "eventUnsubscriptionReply":
            if body_json.get("ok"):
                print(
                    "[S³I] Get a event unsubscribe reply. You have unsubscribed a event with sub id: {0}{1}{2}".format(
                        bcolors.UNDERLINE, body_json.get("subscription-id"), bcolors.ENDC))
                return True
            else:
                print("[S³I] Get a event unsubscribe reply. The given subscription id is not existing")
                return False


def on_keyboard_press(key):
    """
    Callback function when a key of keyboard is pressed

    :param key: trigger key
    :type key: refer to keyboard.Key

    """
    global alt_pressed
    if key == keyboard.Key.alt_l:
        alt_pressed = True
    else:
        alt_pressed = False


def consume_old_msg():
    """
    consume all old messages

    """
    while True:
        body = saegewerk_broker.receive_once(queue=SAEGEWERK_ENDPOINT)
        if body is None:
            return True


if __name__ == "__main__":

    s3i_idp = IdentityProvider(grant_type="client_credentials",
                               identity_provider_url="https://idp.s3i.vswf.dev/",
                               realm="KWH", client_id=SAEGEWERK_CLIENT_ID,
                               client_secret=SAEGEWERK_CLIENT_SECRET)

    access_token = s3i_idp.get_token(TokenType.ACCESS_TOKEN)

    saegewerk_broker = Broker(auth_form='Username/Password', username=" ", password=access_token,
                              host="rabbitmq.s3i.vswf.dev")
    keyboard_listener = keyboard.Listener(on_press=on_keyboard_press)
    keyboard_listener.start()
    consume_old_msg()

    print("=======================================================================================")
    print("Welcome to S³I EventSystem Demo. Please subscribe a event")
    print("=======================================================================================")

    while True:
        rql = input("[S³I] Please enter rql specification [e.g. and(lt(attributes/features/ml40::Composite/targets/Motor/features/ml40::RotationalSpeed/rpm, 2800), gt(attributes/features/ml40::Location/longitude, 6.0345)) ]: \n")
        msg = subscribe_event(filter_expression=rql)
        saegewerk_broker.send(receiver_endpoints=[HARVESTER_ENDPOINT], msg=msg)

        if receive_sub_reply():
            print("[S³I] To unsubscribe the event, please press " + bcolors.WARNING + "alt " + bcolors.ENDC)
            break
        else:
            print("[S³I] invalid rql")
            continue

    while True:
        if not alt_pressed:
            receive_event_msg()
        else:
            print("\n[S³I] Sending the event unsubscribe request")
            msg = unsubscribe_event(subscription_id)
            saegewerk_broker.send(receiver_endpoints=[HARVESTER_ENDPOINT], msg=msg)

            if receive_unsub_reply():
                break
            else:
                continue
