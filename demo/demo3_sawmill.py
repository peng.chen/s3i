from s3i import IdentityProvider, Directory, Broker, ServiceRequest, TokenType
import json
import uuid
import base64
import time
import os


def callback(ch, method, properties, body):
    """
    This function will be called and executed every time that S3I Broker delivers messages to the consuming application (DT). \n
    It waits for a service reply and then stores the replied production data in ../src/demo_received_data. 


    Args:
        ch: channel used during consuming, which binds productor and consumer \n
        method: „method“ contains meta information regarding message delivery: It provides delivery flags, redelivery flags, and a routing key that can be used to acknowledge the message. \n
        properties: properties of queue and exchange which are used during consuming \n
        body: content of message 
    """

    body_str = body.decode('utf8').replace("'", '"').replace("None", "false")
    body_json = json.loads(body_str)
    ch.basic_ack(method.delivery_tag)

    '''
    parse the received BASE64-encoded production data and write it in a file
    '''
    cwd = os.path.dirname(__file__)
    path = os.path.join(cwd, "demo_received_data",
                        "Productiondata.hpr")
    results = body_json["results"]
    for key in results.keys():
        decode = base64.b64decode(results[key])
        file = open(path, 'wb')
        file.write(decode)
        file.close()

    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) +
          "]: Service reply is received and production data stored in " + path)


if __name__ == "__main__":

    """
    Main function: simulate a DT sawmill that sends a service request to a DT forest machine to obtain production data

    """

    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()
                                   ) + "]: DEMO 3, DT saw mill is started (Sägewerk) ")

    '''
    self authentication at S3I Directory with client secret (without username and password)
    grant type "client_credentials"
    '''
    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) +
          "]: Self authentication with known client credentials at S3I IdentityProvider")

    s3i_identity_provider = IdentityProvider(grant_type='client_credentials',
                                             identity_provider_url="https://idp.s3i.vswf.dev/",
                                             realm='KWH', client_id="s3i:2073c475-fee5-463d-bce1-f702bb06f899",
                                             client_secret="2a5bd2a0-0aa0-4276-92ce-ebd3dc9813a0")
    access_token = s3i_identity_provider.get_token(TokenType.ACCESS_TOKEN)

    time.sleep(2)
    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S",
                                   time.localtime()) + "]: Token received.")

    '''
    authentication with JWT in S3I Directory 
    '''

    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()
                                   ) + "]: Authentication with token at S3I Directory")

    s3i_directory = Directory(
        s3i_dir_url="https://dir.s3i.vswf.dev/api/2/", token=access_token)

    '''
    id and endpoint of sender are known 
    '''

    sender = "s3i:2073c475-fee5-463d-bce1-f702bb06f899"
    sender_endpoint = "s3ib://s3i:2073c475-fee5-463d-bce1-f702bb06f899"

    '''
    search the receiver id, then search its endpoint for service getProductionData
    prepare the service request message
    '''

    dt_harvester = s3i_directory.queryAttributeBased(
        key="name", value="Harvester")[0]
    dt_harvester_id = dt_harvester["thingId"]
    all_eps = dt_harvester["attributes"]["allEndpoints"]
    receivers = list()
    receiver_endpoints = list()
    receivers.append(dt_harvester_id)
    for ep in all_eps:
        if "s3ib" in ep:
            receiver_endpoints.append(ep)

    msg_uuid = "s3i:" + str(uuid.uuid4())

    servReq = ServiceRequest()
    servReq.fillServiceRequest(senderUUID=sender, receiverUUIDs=receivers, sender_endpoint=sender_endpoint,
                               serviceType="fml40.Harvester.getProductionData", parameters={}, msgUUID=msg_uuid)
    '''
    authentication with access token (JWT) at S3I Broker and send service request
    '''

    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()
                                   ) + "]: Authentication with access token at S3I Broker")
    demo_saegewerk = Broker(
        auth_form='Username/Password', username=" ", password=access_token, host="rabbitmq.s3i.vswf.dev")

    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) +
          "]: Saw mill sends the service call to a forest machine to obtain production data ")
    demo_saegewerk.send(receiver_endpoints, servReq.msg.__str__())

    '''
    wait for the reply of the service
    '''
    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()
                                   ) + "]: Saw mill waits for the reply of the forest machine")
    demo_saegewerk.receive(queue=sender_endpoint, callback=callback)
