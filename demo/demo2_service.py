from s3i import IdentityProvider, Directory, Broker, ServiceReply, TokenType
import uuid
import json
import jwt
import base64
import time
import os


def callback(ch, method, properties, body):
    """
    This function will be called and executed every time that S3I Broker delivers messages to the consuming application (Service). \n
    It passes a service request message to the service function calculateStockService() and sends a service reply with the result to the requester.

    Args:
        ch: channel used during consuming, which binds productor and consumer \n
        method: „method“ contains meta information regarding message delivery: It provides delivery flags, redelivery flags, and a routing key that can be used to acknowledge the message. \n
        properties: properties of queue and exchange which are used during consuming \n
        body: content of message 
    """

    body_str = body.decode('utf8').replace("'", '"')
    body_json = json.loads(body_str)
    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()
                                   ) + "]: Service has received a new service call")
    ch.basic_ack(method.delivery_tag)

    """
    decode BASE64-encoded parameters 
    """
    decoded_parameters = base64.b64decode(
        body_json["parameters"]["surface"])

    '''
    call the service function
    '''

    results = calculateStockService(
        body_json["serviceType"], decoded_parameters)

    '''
    prepare an S3I-B service reply message 
    '''
    sender = "s3i:ab0717b0-e025-4344-8598-bcca1d3d5d47"
    msg_uuid = "s3i:" + str(uuid.uuid4())
    receivers = list()
    receiver_endpoints = list()
    receivers.append(body_json["sender"])
    receiver_endpoints.append(body_json["replyToEndpoint"])
    service_type = body_json["serviceType"]
    replyingToUUID = body_json["identifier"]

    '''
    configure S3I-B service reply message 
    '''
    serReply = ServiceReply()
    serReply.fillServiceReply(senderUUID=sender,
                              receiverUUIDs=receivers,
                              serviceType=service_type,
                              results=results,
                              msgUUID=msg_uuid,
                              replyingToUUID=replyingToUUID)
    '''
    send the S3I-B service reply message 
    '''
    demo_service.send(receiver_endpoints=receiver_endpoints,
                      msg=serReply.msg.__str__())
    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S",
                                   time.localtime()) + "]: Service has sent the reply back to requester")


def calculateStockService(serviceType, parameters):
    """
    Simulate calculate stock service

    Args:
        parameters(str): decoded parameters value \n
        serviceType(str): the type of service called by the requester

    Returns:
        result of service(str)
    """
    if serviceType == "calculateStock":
        if parameters is not None:
            print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()
                                           ) + "]: Service calculates the request, waiting...")
            time.sleep(3)
            results = {"stock": "123.4"}
    else:
        results = None

    return results


if __name__ == "__main__":

    """
    Main function for demo 2: simulate and perform a calculate stock service by starting a message listener and wait for a service request.
    """

    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()
                                   ) + "]: Demo 2, service is started and waits for request!")

    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) +
          "]: Self authentication with known client credentials at S3I IdentityProvider.")
    time.sleep(2)

    '''
    self authentication at S3I IdentityProvider with client secret (without username and password) 
    with the grant type "client_credentials"
    '''
    s3i_ip = IdentityProvider(grant_type='client_credentials', identity_provider_url="https://idp.s3i.vswf.dev/", realm='KWH',
                              client_id="s3i:ab0717b0-e025-4344-8598-bcca1d3d5d47", client_secret="cd23353e-10df-4f9c-864a-b65b2e8393c0")
    access_token = s3i_ip.get_token(TokenType.ACCESS_TOKEN)
    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S",
                                   time.localtime()) + "]: Token received")

    '''
    own endpoint is known 
    '''
    service_endpoint = "s3ib://s3i:ab0717b0-e025-4344-8598-bcca1d3d5d47-1"

    '''
    Authentication and authorization with access token (JWT) at S3I Broker 
    '''
    demo_service = Broker(
        auth_form='Username/Password', username=" ", password=access_token, host="rabbitmq.s3i.vswf.dev")
    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()
                                   ) + "]: Authentication with access token (JWT) in S3I Broker")

    '''
    start the receive function and wait for an S3I-B service request
    '''
    print("[S3I][" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) +
          "]: Callback function is started. Now waiting for the service request.")

    demo_service.receive(service_endpoint, callback)
