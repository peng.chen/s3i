from s3i import IdentityProvider, TokenType
from pynput import keyboard
import websocket
import time
import datetime
import json
import sys
import requests

# client
CLIENT_NAME = "0efea45f-1f63-4097-8546-8c81dec38e37"
CLIENT_ID = "s3i:"+CLIENT_NAME
CLIENT_SECRET = "82732944-507c-417b-ac20-712b24daef14"

# wheelloader
WHEELLOADER_ID = "7f654f13-11ac-413d-841c-10a8e431fc35"
WHEELLOADER_CLIENT_ID = "s3i:" + WHEELLOADER_ID

# s3i components
S3I_IDP_URL = "https://idp.s3i.vswf.dev/"
S3I_IDP_REALM = "KWH"
S3I_REPO_WS_URL = "wss://ditto.s3i.vswf.dev/ws/2"

# S3I Repository paths
WHEELLOADER_MODIFIED_EVENT_TOPIC = "s3i/" + \
    WHEELLOADER_ID+"/things/twin/events/modified"
AUTOMATIC_REFILLED_MESSAGE_TOPIC = "s3i/" + \
    CLIENT_NAME+"/things/live/messages/refilled/notify"
FILLLEVEL_FEATURE_PATH = "features/id3/properties/currentLevel"

WHEELLOADER_FILLLEVEL_MAX = 300

# variables
id_token = None
webs = None
websocket_open = False
stop_demo = False
console_no_new_line = False
new_filter_input = True
# user input
rql_filter = ""
filter_value = ""


class bcolors:
    """colors for the console log"""
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def print_ts(msg, end='\n'):
    """
    prints the given message 
    :param msg: message to print
    :type msg: string
    """
    global console_no_new_line
    trail = ""
    if end == "\r":
        console_no_new_line = True
    elif console_no_new_line:
        console_no_new_line = False
        trail = "\n"
    print(trail+"[S³I][" + datetime.datetime.fromtimestamp(time.time()
                                                           ).strftime('%H:%M:%S.%f')[:-3]+"] " + msg, end=end)


def input_ts(msg):
    return input("[S³I][" + datetime.datetime.fromtimestamp(time.time()
                                                            ).strftime('%H:%M:%S.%f')[:-3]+"] " + msg)


def authenticate():
    """
    authenticates the client against the S3I IdentityProvider 
    """
    print_ts("Authenticate via S3I IdentityProvider...")
    idp = IdentityProvider(grant_type="client_credentials", client_id=CLIENT_ID,
                           client_secret=CLIENT_SECRET, realm=S3I_IDP_REALM,
                           identity_provider_url=S3I_IDP_URL)
    idp.run_forever(TokenType.ACCESS_TOKEN, on_new_token=on_new_token)


def on_new_token(token):
    """
    safes the refreshed token
    :param token: valid jwt
    :type token: string
    """
    global id_token
    id_token = token


def connect_to_s3i_repo():
    """
    creates a new websocket connection to the S3I Repository
    """
    global webs
    print_ts("Attempt to connect to S3I Repository...")
    webs = websocket.WebSocketApp(S3I_REPO_WS_URL,
                                  header={
                                      "Authorization: Bearer " + id_token},
                                  on_message=on_new_websocket_message,
                                  on_error=on_new_websocket_error,
                                  on_open=on_websocket_connection_opened,
                                  on_close=on_websocket_connection_closed)
    webs.run_forever()


def on_websocket_connection_opened(ws):
    """
    called if the websocket connection is established
    :param ws: websocket object
    :type ws: WebSocketApp
    """
    global websocket_open
    websocket_open = True
    print_ts(bcolors.OKGREEN+"Websocket connection is working"+bcolors.ENDC)
    if new_filter_input:
        ask_user_for_filtering_params()
    else:
        register_for_wheelloader_events()


def ask_user_for_filtering_params():
    """
    asks the user for the filtering parameters and the filtering value
    """
    global new_filter_input
    global rql_filter
    global filter_value
    new_filter_input = False
    space = ' '
    ind = 4
    print_ts("Start monitoring tank filllevel of the wheel loader")
    print_ts("Following filter types are available:")
    print(space*ind, "[x]  -> don't filter events")
    print(space*ind, "[eq] -> equal to a value")
    print(space*ind, "[ge] -> greater than or equal to a value")
    print(space*ind, "[le] -> less than or equal to a value")
    rql_filter = input_ts("Please enter one of these (x, eq, ge, le): ")
    if rql_filter != "x":
        if(rql_filter != "eq" and rql_filter != "ge" and rql_filter != "le"):
            print_ts("No valid filter parameter!")
            sys.exit()
        filter_value = input_ts("Please enter a value to compare: ")
    register_for_wheelloader_events()


def register_for_wheelloader_events():
    """
    sends a websocket message to S3I Repository and register this client for events
    """
    if websocket_open:
        print_ts(
            "Register for wheelloader filllevel events ["+rql_filter+","+filter_value+"]")
        if rql_filter == "x":
            webs.send("START-SEND-EVENTS")
        else:
            webs.send("START-SEND-EVENTS?namespaces="
                      + "s3i"
                      + "&filter="+rql_filter +
                      "(" + FILLLEVEL_FEATURE_PATH + ","
                      + filter_value
                      + ")")
        webs.send("START-SEND-MESSAGES")


def on_new_websocket_message(ws, msg):
    """
    called if a new message is available; checks the topic of the message
    :param ws: websocket object
    :type ws: WebSocketApp
    :param msg: received message
    :type msg: string
    """
    msg = json.loads(msg)
    if msg and "topic" in msg:
        if msg["topic"] == WHEELLOADER_MODIFIED_EVENT_TOPIC:
            on_new_filllevel(msg["value"])
        elif msg["topic"] == AUTOMATIC_REFILLED_MESSAGE_TOPIC:
            on_new_auto_refill(msg["value"])


def on_new_filllevel(msg):
    """
    called if a received message topic is equal to WHEELLOADER_MODIFIED_EVENT_TOPIC
    :param msg: the message from S3I Repository
    :type msg: json
    """
    if "currentLevel" in msg:
        update_progress(int(msg["currentLevel"]))


def on_new_auto_refill(msg):
    """
    called if a received message topic is equal to AUTOMATIC_REFILLED_MESSAGE_TOPIC
    """
    print_ts("Received wheelloader message: " + str(msg))


def update_progress(value):
    """
    updates the tank fill level to the given value
    :param value: the new value
    :type value: int
    """
    print_ts(bcolors.OKBLUE+"Tank level: ("+str(value).zfill(3)+"/" +
             str(WHEELLOADER_FILLLEVEL_MAX)+")"+bcolors.ENDC, end="\r")


def on_new_websocket_error(ws, error):
    print_ts(bcolors.FAIL+"Websocket error: "+error+bcolors.ENDC)


def on_websocket_connection_closed(ws):
    global websocket_open
    print_ts(bcolors.WARNING+"Websocket connection closed"+bcolors.ENDC)
    websocket_open = False
    if not stop_demo:
        print_ts("Try to reestablish connection..."+bcolors.ENDC)
        connect_to_s3i_repo()


def on_key_pressed(key):
    global stop_demo
    if key == keyboard.Key.alt_l:
        print_ts(bcolors.WARNING +
                 "Stop demo and close all connections..."+bcolors.ENDC)
        stop_demo = True
        webs.close()
        sys.exit()


if __name__ == "__main__":
    """
    Main function for S3I Repository websocket demo: Start identity provider and websocket connection

    """
    print_ts(bcolors.UNDERLINE+"Starting S3I Repository Websocket Demo"+bcolors.ENDC)
    print_ts(bcolors.WARNING+"Press 'alt' to end the demo"+bcolors.ENDC)
    listener = keyboard.Listener(on_press=on_key_pressed)
    listener.start()
    authenticate()
    connect_to_s3i_repo()
