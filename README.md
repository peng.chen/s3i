# Documentation
This package contains everything you need to get started with the S3I. The documentation of the S3I package and its demos can be found [here](https://git.rwth-aachen.de/kwh40/s3i/-/jobs/artifacts/master/file/public/html/index.html?job=pages).

# Installation
In order to use the s3i package in you own project, install it 
via pip

```
python -m pip install s3i
```

or use our self hosted 

```
python -m pip install https://git.rwth-aachen.de/kwh40/s3i/-/jobs/artifacts/master/raw/public/s3i-0.5.3-py3-none-any.whl?job=wheel
``` 
